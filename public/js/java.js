$(function () {
    'use strict'
  
    $('[data-toggle="offcanvas"]').on('click', function () {
      $('.hk-offcanvas').toggleClass('open')
    })
  })
$(function(){
    $(".ajaxForm").submit(function(e){
        e.preventDefault();
        var href = $(this).attr("action");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: href,
            data: $(this).serialize(),
            success: function(response){
                if(response.status == "success"){
                    alert("Ваша заявка успешно отправлена !");
					localStorage.removeItem("email");
                    localStorage.removeItem("nick");
                    localStorage.removeItem("anotherInput");
					localStorage.removeItem("check");
					email.value = localStorage.getItem("email");
                    nick.value = localStorage.getItem("nick");
                    anotherInput.value = localStorage.getItem("anotherInput");
					check.checked = false;
                }else{
                    alert("Что-то пошло не так " + response.message);
                }
            }
        });
    });
});
window.onpopstate = function(event) {
  console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));
};

$(document).ready(function () {

    $('.form-submit').click(function (e) {
        e.preventDefault();
        var NameSurname = $('#namesurname').val();
        var Mail = $('#mail').val();
        var Message = $('#message').val();

        if (NameSurname == "" || Mail == "" || Message == "" || !isValidMailAddress($('#mail').val())) { } else {
            $('.form-send-message').css('display', 'block');
            $('.form-container').css('display', 'none');
       
        };
    })

    $('.top-banner, .top-banner-img').css('height', $(window).innerHeight());
    $('.carousel').carousel({
        interval: 112000
    })

    $('#carouselExampleSlidesOnly').carousel();

    $('.carousel').bcSwipe({ threshold: 50 });

    $('.works a').on({
        mouseenter: function () {
            $(this).find('.works-hover').fadeIn();
            $('.works-hover').css('width', $('.works img').innerWidth());
        },
        mouseleave: function () {
            $(this).find('.works-hover').fadeOut();
        }
    });

    if ($(window).width() < 1200) {
        $('.carousel-add').addClass('carousel-item');
        $('.carousel-add').eq(0).addClass('active');
    }
    else {
        $('.carousel-add').removeClass('carousel-item');
    }

    function isValidMailAddress(MailAddress) {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        return pattern.test(MailAddress);
    };

});
